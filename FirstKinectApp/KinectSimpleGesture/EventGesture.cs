﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstKinectApp.KinectSimpleGesture
{
    class EventGesture : EventArgs
    {
		private string name;
		public string Name
		{
			get { return this.name; }
		}

		public EventGesture(string n) : base()
		{
			this.name = n;
		}
    }
}
