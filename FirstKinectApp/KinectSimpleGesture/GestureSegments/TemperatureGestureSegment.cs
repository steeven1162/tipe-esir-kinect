﻿using KinectSimpleGesture;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstKinectApp.KinectSimpleGesture.GestureSegments
{
    class TemperatureGestureSegment
    {
        public class TempSegment1 : IGestureSegment
        {
            public GesturePartResult Update(Skeleton skeleton) 
            {


				if(skeleton.Joints[JointType.ElbowRight].Position.Y < skeleton.Joints[JointType.ShoulderRight].Position.Y)
				{
					if (skeleton.Joints[JointType.HandRight].Position.Y < skeleton.Joints[JointType.ElbowRight].Position.Y)
					{
						Console.WriteLine("coucou");

						if (skeleton.Joints[JointType.HandRight].Position.X > skeleton.Joints[JointType.ElbowRight].Position.X)
						{
							return GesturePartResult.Succeeded;
						}
					}
				}

                return GesturePartResult.Failed;
            }
        }

        public class TempSegment2 : IGestureSegment
        {
            public GesturePartResult Update(Skeleton skeleton)
            {
				if (skeleton.Joints[JointType.ElbowRight].Position.Y < skeleton.Joints[JointType.ShoulderRight].Position.Y)
				{
					if (skeleton.Joints[JointType.HandRight].Position.Y < skeleton.Joints[JointType.ElbowRight].Position.Y)
					{
						if (skeleton.Joints[JointType.HandRight].Position.X < skeleton.Joints[JointType.ShoulderRight].Position.X)
						{
							return GesturePartResult.Succeeded;
						}
					}
				}

                return GesturePartResult.Failed;
            }
        }
    }
}
