﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinectSimpleGesture;
using Microsoft.Kinect;


namespace FirstKinectApp.KinectSimpleGesture.GestureSegments
{
    public class LightsOffGestureSegment1 : IGestureSegment
    {
        GesturePartResult IGestureSegment.Update(Skeleton skeleton)
        {
            //Hand above elbow
            if (skeleton.Joints[JointType.HandRight].Position.Y > skeleton.Joints[JointType.ElbowRight].Position.Y)
            {
                return GesturePartResult.Succeeded;
            }
            else
            {
                return GesturePartResult.Failed;
            }
        }
    }

    public class LightsOffGestureSegment2 : IGestureSegment
    {
        public GesturePartResult Update(Skeleton skeleton)
        {
            //TODO
            //Hand closing
            throw new NotImplementedException();
        }
    }
    public class LightsOffGestureSegment3 : IGestureSegment
    {
        GesturePartResult IGestureSegment.Update(Skeleton skeleton)
        {
            //Hand below elbow
            if (skeleton.Joints[JointType.HandRight].Position.Y < skeleton.Joints[JointType.ElbowRight].Position.Y)
            {
                return GesturePartResult.Succeeded;
            }
            else
            {
                return GesturePartResult.Failed;
            }
        }
    }

    public class LightsOffGestureSegment4 : IGestureSegment
    {
        GesturePartResult IGestureSegment.Update(Skeleton skeleton)
        {
            //TODO
            //Hand opening
            throw new NotImplementedException();
        }
    }
}
