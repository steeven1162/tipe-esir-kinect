﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstKinectApp.KinectSimpleGesture;
using Microsoft.Kinect;
using static FirstKinectApp.KinectSimpleGesture.GestureSegments.TemperatureGestureSegment;

namespace KinectSimpleGesture.Gesture
{
    class TemperatureGesture : Gesture
    {
        readonly int MAX_REFRESH = 50;

        IGestureSegment[] _HeatSegments;
        IGestureSegment[] _CoolSegments;

        int _currentSegment = 0;
        int _frameCount = 0;

        bool actualGestureIsHeat = false;
        bool actualGestureIsCool = false;

        public override event EventHandler GestureRecognized;

        public TemperatureGesture()
        {
            TempSegment1 _TempsSeg1 = new TempSegment1();
            TempSegment2 _TempsSeg2 = new TempSegment2();

            _HeatSegments = new IGestureSegment[]{
				_TempsSeg1,
				_TempsSeg2
			};

            _CoolSegments = new IGestureSegment[]
            {
				_TempsSeg2,
				_TempsSeg1
			};
        }

        public override void Update(Skeleton skeleton)
        {
            IGestureSegment[] _working;

            if (actualGestureIsHeat)
                _working = _HeatSegments;
            else if (actualGestureIsCool)
                _working = _CoolSegments;
            else
            {
                GesturePartResult test = _HeatSegments[0].Update(skeleton);
                if (test == GesturePartResult.Succeeded)
                {
                    _working = _HeatSegments;
                    actualGestureIsHeat = true;
                }
                else
                {
                    _working = _CoolSegments;
                    actualGestureIsCool = true;
                }
            }

            GesturePartResult result = _working[_currentSegment].Update(skeleton);


            if (result == GesturePartResult.Succeeded)
            {
                Console.WriteLine("temp success");
                if (_currentSegment + 1 < _working.Length)
                {
                    _currentSegment++;
                    _frameCount = 0;
                }
                else
                {
                    if (GestureRecognized != null)
                    {
                        if (actualGestureIsHeat)
                        {
                            GestureRecognized(this, new EventGesture("temp_up"));
                        }
                        else
                        {
                            GestureRecognized(this, new EventGesture("temp_down"));
                        }
                        Reset();
                    }
                }
            }
            else if (result == GesturePartResult.Failed && _frameCount > MAX_REFRESH)
            {
                Console.WriteLine("temp fail");
                Reset();
            }
            else
            {
                _frameCount++;
            }
        }

		public override void Reset()
		{
			_currentSegment = 0;
			_frameCount = 0;
			actualGestureIsCool = false;
			actualGestureIsHeat = false;
		}
	}
}
