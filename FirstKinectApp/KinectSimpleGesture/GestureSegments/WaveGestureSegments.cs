﻿using Microsoft.Kinect;

namespace KinectSimpleGesture.GestureSegments
{

	public class WaveSegment1 : IGestureSegment
	{
		/// <summary>
		/// Updates the current gesture.
		/// </summary>
		/// <param name="skeleton">The skeleton.</param>
		/// <returns>A GesturePartResult based on whether the gesture part has been completed.</returns>
		public GesturePartResult Update(Skeleton skeleton)
		{
			// Hand above elbow
			if (skeleton.Joints[JointType.HandLeft].Position.Y > skeleton.Joints[JointType.ElbowLeft].Position.Y)
			{
				// Hand right of elbow
				if (skeleton.Joints[JointType.HandLeft].Position.X > skeleton.Joints[JointType.ElbowLeft].Position.X)
				{
					return GesturePartResult.Succeeded;
				}
			}

			// Hand dropped
			return GesturePartResult.Failed;
		}
	}

	public class WaveSegment2 : IGestureSegment
	{
		/// <summary>
		/// Updates the current gesture.
		/// </summary>
		/// <param name="skeleton">The skeleton.</param>
		/// <returns>A GesturePartResult based on whether the gesture part has been completed.</returns>
		public GesturePartResult Update(Skeleton skeleton)
		{
			// Hand above elbow
			if (skeleton.Joints[JointType.HandLeft].Position.Y > skeleton.Joints[JointType.ElbowLeft].Position.Y)
			{
				// Hand left of elbow
				if (skeleton.Joints[JointType.HandLeft].Position.X < skeleton.Joints[JointType.ElbowLeft].Position.X)
				{
					return GesturePartResult.Succeeded;
				}
			}

			// Hand dropped
			return GesturePartResult.Failed;
		}
	}
}
