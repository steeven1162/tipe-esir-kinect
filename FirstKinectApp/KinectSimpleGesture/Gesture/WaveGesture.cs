﻿using FirstKinectApp;
using FirstKinectApp.KinectSimpleGesture;
using KinectSimpleGesture.Gesture;
using KinectSimpleGesture.GestureSegments;
using Microsoft.Kinect;
using System;
using System.Windows.Media;

namespace KinectSimpleGesture.Gesture
{
	public class WaveGesture : Gesture
	{
		readonly int MAX_REFRESH = 50;

		IGestureSegment[] _segments;

		int _currentSegment = 0;
		int _frameCount = 0;

		public override event EventHandler GestureRecognized;

		public WaveGesture()
		{
			WaveSegment1 waveRightSegment1 = new WaveSegment1();
			WaveSegment2 waveRightSegment2 = new WaveSegment2();

			_segments = new IGestureSegment[]
			{
				waveRightSegment1,
				waveRightSegment2,
				waveRightSegment1,
				waveRightSegment2,
				waveRightSegment1,
				waveRightSegment2
			};
		}

		/// <summary>
		/// Updates the current gesture.
		/// </summary>
		/// <param name="skeleton">The skeleton data.</param>
		public override void Update(Skeleton skeleton)
		{
			GesturePartResult result = _segments[_currentSegment].Update(skeleton);

			if (result == GesturePartResult.Succeeded)
			{
				Console.WriteLine("wave success");
				if (_currentSegment + 1 < _segments.Length)
				{
					_currentSegment++;
					_frameCount = 0;
				}
				else
				{
					if (GestureRecognized != null)
					{
						GestureRecognized(this, new EventGesture("wave"));
						Reset();
					}
				}
			}
			else if (result == GesturePartResult.Failed && _frameCount > MAX_REFRESH)
			{
				Console.WriteLine("wave fail");
				Reset();
			}
			else
			{
				_frameCount++;
			}
		}

		/// <summary>
		/// Resets the current gesture.
		/// </summary>
		public override void Reset()
		{
			_currentSegment = 0;
			_frameCount = 0;
		}

	}
}
