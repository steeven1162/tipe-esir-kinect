﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirstKinectApp.KinectSimpleGesture;
using Microsoft.Kinect;
using static FirstKinectApp.KinectSimpleGesture.GestureSegments.LightGestureSegments;

namespace KinectSimpleGesture.Gesture
{
	class LightGesture : Gesture
    {
		readonly int MAX_REFRESH = 50;

		IGestureSegment[] _lightOnSegments;
		IGestureSegment[] _lightOffSegments;

		int _currentOnSegment = 0;
		int _currentOffSegment = 0;
		int _frameCount = 0;

		public override event EventHandler GestureRecognized;

		public LightGesture()
		{
			LightOnSegment1 lightOnSeg1 = new LightOnSegment1();
			LightOnSegment2 lightOnSeg2 = new LightOnSegment2();
			LightOffSegment1 lightOffSeg1 = new LightOffSegment1();
			LightOffSegment2 lightOffSeg2 = new LightOffSegment2();

			_lightOnSegments = new IGestureSegment[]{
				lightOnSeg1,
				lightOnSeg2
			};

			_lightOffSegments = new IGestureSegment[]{
				lightOffSeg1,
				lightOffSeg2
			};
		}

		public override void Update(Skeleton skeleton)
		{
			GesturePartResult resultOn = _lightOnSegments[_currentOnSegment].Update(skeleton);
			GesturePartResult resultOff = _lightOffSegments[_currentOffSegment].Update(skeleton);


			if (resultOn == GesturePartResult.Succeeded)
			{
				Console.WriteLine("light success");
				if (_currentOnSegment + 1 < _lightOnSegments.Length)
				{
					_currentOnSegment++;
					_frameCount = 0;
				}
				else
				{
					if (GestureRecognized != null)
					{
						GestureRecognized(this, new EventGesture("light_on"));
						Reset();
					}
				}
			}
			else if (resultOn == GesturePartResult.Failed && _frameCount > MAX_REFRESH)
			{
				Console.WriteLine("light fail");
				Reset();
			}
			else
			{
				_frameCount++;
			}

			if (resultOff == GesturePartResult.Succeeded)
			{
				Console.WriteLine("light success");
				if (_currentOffSegment + 1 < _lightOffSegments.Length)
				{
					_currentOffSegment++;
					_frameCount = 0;
				}
				else
				{
					if (GestureRecognized != null)
					{
						GestureRecognized(this, new EventGesture("light_off"));
						Reset();
					}
				}
			}
			else if (resultOff == GesturePartResult.Failed && _frameCount > MAX_REFRESH)
			{
				Console.WriteLine("light fail");
				Reset();
			}
			else
			{
				_frameCount++;
			}
		}

		public override void Reset()
		{
			_currentOnSegment = 0;
			_currentOffSegment = 0;
			_frameCount = 0;
		}
	}


}
