﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using FirstKinectApp.KinectSimpleGesture;
using KinectSimpleGesture.Gesture;
using Microsoft.Kinect;
using static System.Net.Mime.MediaTypeNames;

namespace FirstKinectApp
{

	/// <summary>
	/// Logique d'interaction pour MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private KinectSensor sensor;
		private ColorImageFormat lastImageFormat = ColorImageFormat.Undefined;
		private Byte[] pixelData;
		private byte[] colorPixels;
		private WriteableBitmap outputImage;
		private WriteableBitmap colorBitmap;
		private DepthImagePixel[] depthPixels;
		private static readonly int Bgr32BytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel) / 8;
		static WaveGesture _waveGesture = new WaveGesture();
		static LightGesture _lightGesture = new LightGesture();
		static TemperatureGesture _tempGesture = new TemperatureGesture();
		private DispatcherTimer timer = new DispatcherTimer();

		public MainWindow()
		{
			InitializeComponent();
			Loaded += new RoutedEventHandler(Window_Loaded);
		}

		private void Window_Loaded(Object sender, RoutedEventArgs e)
		{
			sensor = KinectSensor.KinectSensors[0];


			if (sensor.Status == KinectStatus.Connected)
			{
				sensor.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
				sensor.DepthStream.Enable();
				sensor.SkeletonStream.Enable();

				sensor.ColorFrameReady += new EventHandler<ColorImageFrameReadyEventArgs>(sensor_ColorFrameReady);
				sensor.DepthFrameReady += new EventHandler<DepthImageFrameReadyEventArgs>(sensor_DepthFrameReady);
				sensor.SkeletonFrameReady += Sensor_SkeletonFrameReady;

				_waveGesture.GestureRecognized += Gesture_GestureRecognized;
				_lightGesture.GestureRecognized += Gesture_GestureRecognized;
				_tempGesture.GestureRecognized += Gesture_GestureRecognized;

				sensor.Start();
			}
			

		}

		private void sensor_DepthFrameReady(object sender, DepthImageFrameReadyEventArgs e)
		{
			using (DepthImageFrame depthFrame = e.OpenDepthImageFrame())
			{
				if (depthFrame != null)
				{
					this.depthPixels = new DepthImagePixel[this.sensor.DepthStream.FramePixelDataLength];
					this.colorPixels = new byte[this.sensor.DepthStream.FramePixelDataLength * sizeof(int)];
					this.colorBitmap = new WriteableBitmap(this.sensor.DepthStream.FrameWidth, this.sensor.DepthStream.FrameHeight, 96.0, 96.0, PixelFormats.Bgr32, null);
					this.video_2.Source = this.colorBitmap;

					depthFrame.CopyDepthImagePixelDataTo(this.depthPixels);

					int minDepth = depthFrame.MinDepth;
					int maxDepth = depthFrame.MaxDepth;

					int colorPixelIndex = 0;
					for (int i = 0; i < this.depthPixels.Length; ++i)
					{
						short depth = depthPixels[i].Depth;
						
						byte intensity = (byte) (depth >= minDepth && depth <= maxDepth ? depth : 0);

						this.colorPixels[colorPixelIndex++] = intensity;
						this.colorPixels[colorPixelIndex++] = intensity;                    
						this.colorPixels[colorPixelIndex++] = intensity;

						++colorPixelIndex;
					}

					this.colorBitmap.WritePixels(
						new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
						this.colorPixels,
						this.colorBitmap.PixelWidth * sizeof(int),
						0);
				}
			}
		}

		private void sensor_ColorFrameReady(Object sender, ColorImageFrameReadyEventArgs e)
		{
			using (ColorImageFrame imageFrame = e.OpenColorImageFrame())
			{
				if (imageFrame != null)
				{
					bool newFormat = this.lastImageFormat != imageFrame.Format;

					if (newFormat)
					{
						this.pixelData = new Byte[imageFrame.PixelDataLength];
					}

					imageFrame.CopyPixelDataTo(pixelData);

					if (newFormat)
					{
						this.video_1.Visibility = Visibility.Visible;
						this.outputImage = new WriteableBitmap(
							imageFrame.Width,
							imageFrame.Height,
							96,
							96,
							PixelFormats.Bgr32,
							null);

						this.video_1.Source = this.outputImage;
					}

					this.outputImage.WritePixels(
						new Int32Rect(0, 0, imageFrame.Width, imageFrame.Height),
						this.pixelData,
						imageFrame.Width * Bgr32BytesPerPixel,
						0);

					this.lastImageFormat = imageFrame.Format;
				}
			}
		}

		static void Sensor_SkeletonFrameReady(object sender, SkeletonFrameReadyEventArgs e)
		{
			using (var frame = e.OpenSkeletonFrame())
			{
				if (frame != null)
				{
					Skeleton[] skeletons = new Skeleton[frame.SkeletonArrayLength];

					frame.CopySkeletonDataTo(skeletons);

					if (skeletons.Length > 0)
					{
						var user = skeletons.Where(u => u.TrackingState == SkeletonTrackingState.Tracked).FirstOrDefault();
						
						if (user != null)
						{
							_lightGesture.Update(user);
							_waveGesture.Update(user);
							_tempGesture.Update(user);
						}
					}
				}
			}
		}

		void Gesture_GestureRecognized(object sender, EventArgs e)
		{
			timer.Stop();
			EventGesture ev = (EventGesture) e;
			Border border = null;
			switch(ev.Name)
			{
				case "light_on":
					this.light_on_border.Background = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
					border = this.light_on_border;
					break;
				case "light_off":
					this.light_off_border.Background = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
					border = this.light_off_border;
					break;
				case "wave":
					this.wave_border.Background = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
					border = this.wave_border;
					break;
				case "temp_up":
					this.temp_up_border.Background = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
					border = this.temp_up_border;
					break;
				case "temp_down":
					this.temp_down_border.Background = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
					border = this.temp_down_border;
					break;
			}

			timer.Interval = TimeSpan.FromSeconds(1);
			timer.Tick += (s, ee) => RestoreBlankBackground(s, ee, border);
			timer.Start();
		}

		private void RestoreBlankBackground(object sender, EventArgs e, Border b)
		{
			b.Background = new SolidColorBrush(Color.FromArgb(0, 255, 0, 0));
			timer.Stop();
		}

	}

}
