﻿using KinectSimpleGesture;
using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstKinectApp.KinectSimpleGesture.GestureSegments
{
    class LightGestureSegments
	{
		public class LightOnSegment1 : IGestureSegment
		{
			public GesturePartResult Update(Skeleton skeleton)
			{
				// Right Hand below elbow
				if (skeleton.Joints[JointType.HandRight].Position.Y < skeleton.Joints[JointType.ElbowRight].Position.Y)
				{
                    if (skeleton.Joints[JointType.HandRight].Position.Y < skeleton.Joints[JointType.Head].Position.Y)
                    {
                        //TODO : Check if hand is closed
                        return GesturePartResult.Succeeded;
                    }
				}

				// Hand dropped
				return GesturePartResult.Failed;
			}
		}

		public class LightOnSegment2 : IGestureSegment
		{
			public GesturePartResult Update(Skeleton skeleton)
			{
				// Right Hand above elbow
				if (skeleton.Joints[JointType.HandRight].Position.Y > skeleton.Joints[JointType.ElbowRight].Position.Y)
				{
                    if (skeleton.Joints[JointType.HandRight].Position.Y > skeleton.Joints[JointType.Head].Position.Y)
                    {
                        //TODO : Check if hand is closed
                        return GesturePartResult.Succeeded;
                    }
                }

				// Hand dropped
				return GesturePartResult.Failed;
			}
		}

		public class LightOffSegment1 : IGestureSegment
		{
			public GesturePartResult Update(Skeleton skeleton)
			{
				// Right Hand below elbow
				if (skeleton.Joints[JointType.HandLeft].Position.Y < skeleton.Joints[JointType.ElbowLeft].Position.Y)
				{
					if (skeleton.Joints[JointType.HandLeft].Position.Y < skeleton.Joints[JointType.Head].Position.Y)
					{
						//TODO : Check if hand is closed
						return GesturePartResult.Succeeded;
					}
				}

				// Hand dropped
				return GesturePartResult.Failed;
			}
		}

		public class LightOffSegment2 : IGestureSegment
		{
			public GesturePartResult Update(Skeleton skeleton)
			{
				// Right Hand above elbow
				if (skeleton.Joints[JointType.HandLeft].Position.Y > skeleton.Joints[JointType.ElbowLeft].Position.Y)
				{
					if (skeleton.Joints[JointType.HandLeft].Position.Y > skeleton.Joints[JointType.Head].Position.Y)
					{
						//TODO : Check if hand is closed
						return GesturePartResult.Succeeded;
					}
				}

				// Hand dropped
				return GesturePartResult.Failed;
			}
		}

	}
}
