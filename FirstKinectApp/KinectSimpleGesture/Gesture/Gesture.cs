﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectSimpleGesture.Gesture
{
	abstract public class Gesture
	{

		public abstract event EventHandler GestureRecognized;

		/// <summary>
		/// Updates the current gesture.
		/// </summary>
		/// <param name="skeleton">The skeleton data.</param>
		abstract public void Update(Skeleton skeleton);

		/// <summary>
		/// Resets the current gesture.
		/// </summary>
		abstract public void Reset();
	}
}
